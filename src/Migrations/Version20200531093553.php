<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200531093553 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event_sport ADD CONSTRAINT FK_5B9E5EFEAC78BCF8 FOREIGN KEY (sport_id) REFERENCES sport (id)');
        $this->addSql('CREATE INDEX IDX_5B9E5EFEAC78BCF8 ON event_sport (sport_id)');
        $this->addSql('ALTER TABLE user_info DROP FOREIGN KEY FK_B1087D9E67B3B43D');
        $this->addSql('ALTER TABLE user_info ADD CONSTRAINT FK_B1087D9E67B3B43D FOREIGN KEY (users_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event_sport DROP FOREIGN KEY FK_5B9E5EFEAC78BCF8');
        $this->addSql('DROP INDEX IDX_5B9E5EFEAC78BCF8 ON event_sport');
        $this->addSql('ALTER TABLE user_info DROP FOREIGN KEY FK_B1087D9E67B3B43D');
        $this->addSql('ALTER TABLE user_info ADD CONSTRAINT FK_B1087D9E67B3B43D FOREIGN KEY (users_id) REFERENCES user (id)');
    }
}
