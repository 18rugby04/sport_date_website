<?php

namespace App\Form;

use App\Entity\EventSportSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventSportSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codePostal', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Code Postal'
                ]
            ])
            ->add('typeSport', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Sport choisi'
                ]
            ]);
        //     ,
        //     EntityType::class,
        //     [
        //         'class' => Sport::class,
        //         'placeholder' => 'Choisir une option',
        //         'choice_label' => 'sportName',
        //         'expanded' => false,
        //         'multiple' => false,
        //         'attr' => [
        //             'required' => false
        //         ]
        //     ]
        // );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EventSportSearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
