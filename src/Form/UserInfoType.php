<?php

namespace App\Form;

use App\Entity\Sport;
use App\Entity\UserInfo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('zipCode', TextType::class, [
                'attr' => [
                    'placeholder' => "ex: 07190"
                ],
                'label' => 'Code Postal'
            ])
            ->add('City', TextType::class, [
                'attr' => [
                    'placeholder' => "ex: Paris"
                ],
                'label' => 'Ville'
            ])
            ->add(
                'favoriteSport',
                EntityType::class,
                [
                    'class' => Sport::class,
                    'choice_label' => 'sportName',
                    'expanded' => false,
                    'multiple' => false
                ],
                [
                    'attr' => [
                        'placeholder' => "ex: Rugby"
                    ],
                    'label' => 'Sport favori'
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserInfo::class,
        ]);
    }
}
