<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserEditType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MyAccountController extends AbstractController
{
    /**
     * @Route("/profile/myaccount", name="my_account")
     */
    public function index()
    {
        return $this->render('my_account/index.html.twig', [
            'controller_name' => 'MyAccountController',
        ]);
    }

    /**
     * @Route("/profile/{id}/edit", name="user_edit_profile")
     */
    public function user_edit_profile(User $user, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(UserEditType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success', 'Modifié avec succès');

            return $this->redirectToRoute('my_account');
        }

        return $this->render(
            'my_account/profile_user_edit.html.twig',
            [
                'user' => $user,
                'form' => $form->createView()
            ]
        );
    }
}
