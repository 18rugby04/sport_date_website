<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Sport;
use App\Entity\EventSport;
use App\Form\EventSportType;
use App\Entity\EventSportSearch;
use App\Form\EventSportSearchType;
use App\Repository\HomeRepository;
use App\Repository\UserRepository;
use App\Repository\UserInfoRepository;
use App\Repository\EventSportRepository;
use App\Repository\SportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EventSportController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, EventSportRepository $repo_eventSport, HomeRepository $repo_home, UserRepository $repo_user)
    {
        $eventSports = $repo_eventSport->findAll();
        $home = $repo_home->findAll();
        $lastThree = $repo_eventSport->findLastThree();
        return $this->render('event_sport/index.html.twig', [
            'controller_name' => 'EventSportController',
            'eventSports' => $eventSports,
            'home' => $home,
            'lasts' => $lastThree,
        ]);
    }


    /**
     * @Route("/profile/event_sport/search", name="event_sport_search")
     */
    public function showSportBySearch(Request $request, EventSportRepository $repo, SportRepository $sport_repo)
    {
        $eventSports = [];
        $search = new EventSportSearch();
        $form = $this->createForm(EventSportSearchType::class, $search);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $eventSports = $repo->findAllEventSport($search);
        } else {
            $eventSports = $repo->findAll();
        }



        return $this->render('event_sport/event_sport_search.html.twig', [
            'controller_name' => 'EventSportController',
            'eventSports' => $eventSports,
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/profile/event_sport/new", name="event_sport_create")
     *  @Route("/profile/event_sport/{id}/edit", name="event_sport_edit")
     */
    public function form(EventSport $eventSport = null, Request $request, EntityManagerInterface $manager, UserRepository $user_repo)
    {
        if (!$eventSport) {
            $eventSport = new EventSport();
        }

        $user = $this->getUser()->getId();
        $monuser = $this->getDoctrine()->getRepository(User::class)->find($user);


        $form = $this->createForm(EventSportType::class, $eventSport);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$eventSport->getId()) {
                $eventSport->setCreatedAt(new \DateTime())
                    ->setCreator($monuser);
            }
            $manager->persist($eventSport);
            $manager->flush();
            return $this->redirectToRoute('event_sport_show', ['id' => $eventSport->getId()]);
        }

        return $this->render('event_sport/event_sport_create.html.twig', [
            'formEventSport' => $form->createView(),
            'editMode' => $eventSport->getId() !== null
        ]);
    }

    /**
     * @Route("/profile/event_sport/{id}", name="event_sport_show")
     */
    public function event_sport_show(EventSport $eventSport)
    {
        return $this->render('event_sport/event_sport_show.html.twig', [
            'eventSport' => $eventSport
        ]);
    }
}
