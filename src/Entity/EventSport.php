<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\EventSportRepository;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EventSportRepository::class)
 */
class EventSport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min = 8,
     *      minMessage = "Le titre est trop court"
     * )  
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Url(message="Ceci n'est pas une url")
     */
    private $image;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=2, max=1000, minMessage = "La description est trop courte", maxMessage = "La description est trop longue")
     */
    private $descriptionEvent;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\GreaterThan("today")
     */
    private $eventDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    // /**
    //  * @ORM\Column(type="string", length=255)
    //  * @Assert\Length(min=2, max=255, minMessage = "Le nom du créateur de l'événement est trop court", maxMessage = "Le nom du créateur de l'événement est trop long")
    //  */
    // private $author;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[0-9]{5}$/", message="Ceci n'est pas un code postal")
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[[:alpha:]]([-' ]?[[:alpha:]])*$/", message="Le nom de la commune n'est pas correct")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity=Sport::class, inversedBy="eventSports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sports;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="eventSports")
     */
    private $creator;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescriptionEvent(): ?string
    {
        return $this->descriptionEvent;
    }

    public function setDescriptionEvent(string $descriptionEvent): self
    {
        $this->descriptionEvent = $descriptionEvent;

        return $this;
    }

    public function getEventDate(): ?\DateTimeInterface
    {
        return $this->eventDate;
    }

    public function setEventDate(\DateTimeInterface $eventDate): self
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getSport(): ?Sport
    {
        return $this->sports;
    }

    public function setSport(?Sport $sports): self
    {
        $this->sports = $sports;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }
}
