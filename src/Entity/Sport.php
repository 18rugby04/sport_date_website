<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\SportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SportRepository::class)
 */
class Sport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min = 2,
     *      minMessage = "Le nom du sport est trop court",
     * max = 255, maxMessage = "Le nom du sport est trop long"
     * )  
     */
    private $sportName;

    /**
     * @ORM\OneToMany(targetEntity=UserInfo::class, mappedBy="favoriteSport", orphanRemoval=true)
     */
    private $userInfos;

    /**
     * @ORM\OneToMany(targetEntity=EventSport::class, mappedBy="sport", orphanRemoval=true)
     */
    private $eventSports;

    public function __construct()
    {
        $this->userInfos = new ArrayCollection();
        $this->eventSports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSportName(): ?string
    {
        return $this->sportName;
    }

    public function setSportName(string $sportName): self
    {
        $this->sportName = $sportName;

        return $this;
    }

    /**
     * @return Collection|UserInfo[]
     */
    public function getUserInfos(): Collection
    {
        return $this->userInfos;
    }

    public function addUserInfo(UserInfo $userInfo): self
    {
        if (!$this->userInfos->contains($userInfo)) {
            $this->userInfos[] = $userInfo;
            $userInfo->setFavoriteSport($this);
        }

        return $this;
    }

    public function removeUserInfo(UserInfo $userInfo): self
    {
        if ($this->userInfos->contains($userInfo)) {
            $this->userInfos->removeElement($userInfo);
            // set the owning side to null (unless already changed)
            if ($userInfo->getFavoriteSport() === $this) {
                $userInfo->setFavoriteSport(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EventSport[]
     */
    public function getEventSports(): Collection
    {
        return $this->eventSports;
    }

    public function addEventSport(EventSport $eventSport): self
    {
        if (!$this->eventSports->contains($eventSport)) {
            $this->eventSports[] = $eventSport;
            $eventSport->setSport($this);
        }

        return $this;
    }

    public function removeEventSport(EventSport $eventSport): self
    {
        if ($this->eventSports->contains($eventSport)) {
            $this->eventSports->removeElement($eventSport);
            // set the owning side to null (unless already changed)
            if ($eventSport->getSport() === $this) {
                $eventSport->setSport(null);
            }
        }

        return $this;
    }
}
