<?php

namespace App\Entity;

class EventSportSearch
{

    /**
     * @var string|null
     */
    private $codePostal;

    /**
     * @var string|null
     */
    private $typeSport;

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePOstal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getTypeSport(): ?string
    {
        return $this->typeSport;
    }

    public function setTypeSport(string $typeSport): self
    {
        $this->typeSport = $typeSport;

        return $this;
    }
}
